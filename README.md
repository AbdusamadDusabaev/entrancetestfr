# Инструкция по API MailingList #

## Используемый стек технологий ##
API написано и использованием фреймворков Django и Django Rest Framework. 
<br>Фоновые задачи реализуются через Celery + Redis.
<br>Также используются дополнительные библиотеки, такие как schedule, freezegun, requests, SQLAlchemy 

<br><b><i>Полный список зависимостей вы можете посмотреть в файле requirements.txt</i></b>

## Установка и запуск проекта ##
Для запуска проекта необходимо выполнить следующие действия:

1. Установить исходный код проекта
2. Установить зависимости проекта. Это можно сделать из корневой директории проекта командой 


    pip3 install -r requirements.txt


3. Создать и выполнить миграции в Django-проекте. Для этого необходимо войти в корневую директорию Django проекта (там, где лежит файл manage.py) и ввести следующие команды:


    python3 manage.py makemigrations
    python3 manage.py migrate


4. Запустить Redis. Это можно сделать несколькими командами (зависит от способа установки Redis и ОС), например:


    brew services start redis


5. Запустить Celery Worker командой:


    celery -A mailing-list worker -l info


6. Запустить локальный сервер из корневой директории Django-проекта командой

    
    python3 manage.py runserver


7. Подзравляю! Вы запустили проект на локальной машине. Сервер работает по адресу localhost:8000 или 127.0.0.1:8000


## Примечание ##
Помимо основного задания также были выполнены дополнительные задания под номерами: <b>1, 5, 6, 8, 9, 11, 12</b>
