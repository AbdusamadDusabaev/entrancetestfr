import datetime
import pathlib


MAILING_LISTS_LOG_DIR_NAME: pathlib.Path = pathlib.Path("logs", "mailing_lists")
CLIENTS_LOG_DIR_NAME: pathlib.Path = pathlib.Path("logs", "clients")
MESSAGES_LOG_DIR_NAME: pathlib.Path = pathlib.Path("logs", "messages")


def write_mailing_list_log(mailing_list_id: int | str, log: str) -> None:
    general_log_name: pathlib.Path = pathlib.Path("logs", "mailing-lists-general-log.txt")
    log_name: pathlib.Path = pathlib.Path(MAILING_LISTS_LOG_DIR_NAME, f"{mailing_list_id}.txt")
    log_string: str = f"[{datetime.datetime.now()} | {mailing_list_id}] {log}\n"
    write_log(log_name=log_name, general_log_name=general_log_name, log_string=log_string)


def write_client_log(client_id: int | str, log: str) -> None:
    general_log_name: pathlib.Path = pathlib.Path("logs", "clients-general-log.txt")
    log_name: pathlib.Path = pathlib.Path(CLIENTS_LOG_DIR_NAME, f"{client_id}.txt")
    log_string: str = f"[{datetime.datetime.now()} | {client_id}] {log}\n"
    write_log(log_name=log_name, general_log_name=general_log_name, log_string=log_string)


def write_message_log(message_id: int | str, log: str) -> None:
    general_log_name: pathlib.Path = pathlib.Path("logs", "messages-general-log.txt")
    log_name: pathlib.Path = pathlib.Path(MESSAGES_LOG_DIR_NAME, f"{message_id}.txt")
    log_string: str = f"[{datetime.datetime.now()} | {message_id}] {log}\n"
    write_log(log_name=log_name, general_log_name=general_log_name, log_string=log_string)


def write_log(log_name: pathlib.Path, general_log_name: pathlib.Path, log_string: str) -> None:
    with open(log_name, "a", encoding="utf-8") as file:
        file.write(log_string)

    with open(general_log_name, "a", encoding="utf-8") as file:
        file.write(log_string)
