import re
from typing import Any

from django.db import models
from django.core.exceptions import ValidationError


def mobile_operator_code_validator(mobile_operator_code: str) -> None:
    if len(mobile_operator_code) == 3 and mobile_operator_code.isdigit():
        return None
    raise ValidationError(
        message="Некорректный код мобильного оператора. "
                "Код мобильного оператора должен состоять из 3 цифр в формате XXX"
    )


class Filter(models.Model):
    mobile_operator_code = models.CharField(
        max_length=3, validators=[mobile_operator_code_validator], verbose_name="Код мобильного оператора"
    )
    tag = models.CharField(max_length=50, verbose_name="Тег")

    def __str__(self) -> str:
        return f"Фильтр (Код моб. оператора: {self.mobile_operator_code}; Тег: {self.tag})"

    class Meta:
        verbose_name: str = "Фильтр"
        verbose_name_plural: str = "Фильтры"
        ordering: list[str] = ["-pk"]
        default_related_name: str = "filter"

    def log_str(self) -> str:
        result_str: str = f"mobile_operator_code = {self.mobile_operator_code}; tag = {self.tag}"
        return result_str


class MailingList(models.Model):
    start_time = models.DateTimeField(verbose_name="Дата и время запуска рассылки")
    text = models.TextField(verbose_name="Текст сообщения")
    filter = models.ForeignKey(Filter, on_delete=models.PROTECT, verbose_name="Фильтр")
    stop_time = models.DateTimeField(verbose_name="Дата и время окончания рассылки")
    created_time = models.DateField(auto_now_add=True, verbose_name="Дата создания")
    start_time_interval = models.TimeField(default="00:00:00", verbose_name="Начало временного интервала")
    stop_time_interval = models.TimeField(default="23:59:00", verbose_name="Конец временного интервала")

    def __str__(self) -> str:
        return f"Рассылка (ID: {self.pk}; Time: {self.start_time} - {self.stop_time})"

    class Meta:
        verbose_name: str = "Рассылка"
        verbose_name_plural: str = "Рассылки"
        default_related_name: str = "mailing_list"
        ordering: list[str] = ["-pk"]

    def log_str(self) -> str:
        result_str: str = f"start_time = {self.start_time}; " \
                          f"stop_time = {self.stop_time}; " \
                          f"start_time_interval = {self.start_time_interval}; " \
                          f"stop_time_interval = {self.stop_time_interval}; " \
                          f"text = {self.text}; " \
                          f"filter = Filter({self.filter.log_str()})"
        return result_str

    def short_text(self) -> str:
        if len(str(self.text)) > 100:
            result_short_text: str = str(self.text)[:100] + "..."
        else:
            result_short_text: str = str(self.text)
        return result_short_text

    short_text.__name__ = "Текст"

    def mobile_operator_code(self) -> str:
        return f"{self.filter.mobile_operator_code}"

    mobile_operator_code.__name__ = "Код мобильного оператора"

    def tag(self) -> str:
        return f"{self.filter.tag}"

    tag.__name__ = "Тег"

    def amount_sent_and_received_messages(self) -> int:
        amount_messages_of_status_sent_and_received: int = len(
            Message.objects.filter(mailing_list=self.pk).filter(status="Отправлен и доставлен").all()
        )
        return amount_messages_of_status_sent_and_received

    amount_sent_and_received_messages.__name__ = "Отправлены и доставлены"

    def amount_sent_and_wait_messages(self) -> int:
        amount_messages_of_status_sent_and_wait: int = len(
            Message.objects.filter(mailing_list=self.pk).
            filter(status="Отправлен, ожидаем ответ от стороннего сервиса").all()
        )
        return amount_messages_of_status_sent_and_wait

    amount_sent_and_wait_messages.__name__ = "Ожидают отправки"

    def amount_not_sent_messages(self) -> int:
        amount_messages_of_status_not_sent: int = len(
            Message.objects.filter(mailing_list=self.pk).filter(
                status="Отправлен, но не доставлен. Сообщение не успело дойти до финального времени рассылки"
            ).all()
        )
        return amount_messages_of_status_not_sent

    amount_not_sent_messages.__name__ = "Отправлены, но не доставлены"

    def amount_messages(self) -> dict:
        amount_messages_of_status_sent_and_received: int = self.amount_sent_and_received_messages()
        amount_messages_of_status_sent_and_wait: int = self.amount_sent_and_wait_messages()
        amount_messages_of_status_not_sent: int = self.amount_not_sent_messages()

        result_amount_messages: dict[str, int] = {
            "Отправлен и доставлен": amount_messages_of_status_sent_and_received,
            "Отправлен, ожидаем ответ от стороннего сервиса": amount_messages_of_status_sent_and_wait,
            "Отправлен, но не доставлен. Сообщение не успело дойти до финального времени рассылки":
                amount_messages_of_status_not_sent
        }

        return result_amount_messages

    def messages(self) -> list:
        all_messages: list[Message] = Message.objects.filter(mailing_list=self).all()
        messages_statistic: list[dict[str, Any]] = [
            {
                "send_time": message.send_time,
                "status": message.status,
                "client": {
                    "id": message.client.pk,
                    "phone_number": message.client.phone_number,
                    "mobile_operator_code": message.client.mobile_operator_code,
                    "timezone": message.client.timezone,
                    "tag": message.client.tag
                }
            }
            for message in all_messages
        ]

        return messages_statistic


def phone_validator(phone_number: str) -> None:
    if len(phone_number) == 11 and phone_number.isdigit():
        return None
    raise ValidationError(
        message="Некорректный номер телефона. Номер телефона должен состоять из 11 цифр в формате XXXXXXXXXXX"
    )


def time_zone_validator(time_zone: str) -> None:
    if re.match(pattern=r"UTC[+-]\d\d:\d\d$", string=time_zone) is not None:
        return None
    raise ValidationError(
        message="Некорректное значение часового пояса. "
                "Часовой пояс должен быть передан в формате UTC+XX:XX или UTC-XX:XX"
    )


class Client(models.Model):
    phone_number = models.CharField(max_length=11, validators=[phone_validator], verbose_name="Номер телефона")
    mobile_operator_code = models.CharField(
        max_length=3, validators=[mobile_operator_code_validator], verbose_name="Код мобильного оператора"
    )
    tag = models.CharField(max_length=50, verbose_name="Тег")
    timezone = models.CharField(max_length=10, validators=[time_zone_validator], verbose_name="Часовой пояс")

    def __str__(self) -> str:
        return f"Клиент: {self.phone_number} ({self.tag})"

    class Meta:
        verbose_name: str = 'Клиент'
        verbose_name_plural: str = "Клиенты"
        ordering: list[str] = ["-pk"]
        default_related_name: str = "client"

    def log_str(self) -> str:
        result_str: str = f"phone_number = {self.phone_number}; " \
                          f"mobile_operator_code = {self.mobile_operator_code}; " \
                          f"timezone = {self.timezone}; " \
                          f"tag = {self.tag}"
        return result_str


MESSAGE_STATUS_CHOICES: list[tuple[str, str]] = [
    (
        "Отправлен и доставлен",
        "Отправлен и доставлен"
    ),

    (
        "Отправлен, ожидаем ответ от стороннего сервиса",
        "Отправлен, ожидаем ответ от стороннего сервиса"
    ),

    (
        "Отправлен, но не доставлен. Сообщение не успело дойти до финального времени рассылки",
        "Отправлен, но не доставлен. Сообщение не успело дойти до финального времени рассылки"
    ),
]


class Message(models.Model):
    send_time = models.DateTimeField(auto_now_add=True, verbose_name="Дата и время отправки")
    status = models.CharField(max_length=100, choices=MESSAGE_STATUS_CHOICES, verbose_name="Статус сообщения")
    mailing_list = models.ForeignKey(MailingList, on_delete=models.CASCADE, verbose_name="ID Рассылки")
    client = models.ForeignKey(Client, on_delete=models.CASCADE, verbose_name="ID Клиента")

    def __str__(self) -> str:
        return f"Сообщение (ID: {self.pk}; Mailing List ID: {self.mailing_list})"

    class Meta:
        verbose_name: str = "Сообщение"
        verbose_name_plural: str = "Сообщения"
        default_related_name: str = "message"
        ordering: list[str] = ["-pk"]

    def log_str(self) -> str:
        result_str: str = f"send_time = {self.send_time}; " \
                          f"status = {self.status}; " \
                          f"mailing_list = MailingList({self.mailing_list.log_str()}); " \
                          f"client = Client({self.client.log_str()});"
        return result_str
