# Generated by Django 4.2.1 on 2023-05-06 05:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_alter_message_mailing_list'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='timezone',
            field=models.CharField(max_length=10, verbose_name='Часовой пояс'),
        ),
    ]
