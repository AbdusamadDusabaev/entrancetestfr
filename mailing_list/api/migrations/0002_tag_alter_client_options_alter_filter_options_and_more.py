# Generated by Django 4.2.1 on 2023-05-05 17:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50, verbose_name='Название')),
                ('description', models.TextField(verbose_name='Описание')),
            ],
            options={
                'verbose_name': 'Тег',
                'verbose_name_plural': 'Теги',
                'ordering': ['-pk'],
            },
        ),
        migrations.AlterModelOptions(
            name='client',
            options={'default_related_name': 'client', 'ordering': ['-pk'], 'verbose_name': 'Клиент', 'verbose_name_plural': 'Клиенты'},
        ),
        migrations.AlterModelOptions(
            name='filter',
            options={'default_related_name': 'filter', 'ordering': ['-pk'], 'verbose_name': 'Фильтр', 'verbose_name_plural': 'Фильтры'},
        ),
        migrations.AlterField(
            model_name='client',
            name='tag',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='api.tag', verbose_name='Тег'),
        ),
        migrations.AlterField(
            model_name='filter',
            name='tag',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='api.tag', verbose_name='Тег'),
        ),
    ]
