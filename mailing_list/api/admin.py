from django.contrib import admin
from .models import MailingList, Filter, Client, Message


@admin.register(MailingList)
class MailingListAdmin(admin.ModelAdmin):
    list_display: list[str] = ["id", "created_time", "start_time", "stop_time", "start_time_interval",
                               "stop_time_interval", "mobile_operator_code", "tag", "short_text",
                               "amount_sent_and_received_messages", "amount_sent_and_wait_messages",
                               "amount_not_sent_messages"]
    list_display_links: list[str] = ["id", "start_time", "stop_time"]
    list_filter: list[str] = ["start_time", "stop_time", "filter"]
    search_fields: list[str] = ["id", "start_time", "stop_time", "filter"]


@admin.register(Filter)
class FilterAdmin(admin.ModelAdmin):
    list_display: list[str] = ["id", "mobile_operator_code", "tag"]
    list_display_links: list[str] = ["id", "mobile_operator_code", "tag"]
    list_filter: list[str] = ["mobile_operator_code", "tag"]
    search_fields: list[str] = ["id", "mobile_operator_code", "tag"]


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display: list[str] = ["id", "phone_number", "mobile_operator_code", "tag", "timezone"]
    list_display_links: list[str] = ["id", "phone_number"]
    list_filter: list[str] = ["mobile_operator_code", "tag", "timezone"]
    search_fields: list[str] = ["id", "phone_number", "mobile_operator_code", "tag", "timezone"]


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display: list[str] = ["id", "mailing_list", "send_time", "status", "client"]
    list_display_links: list[str] = ["id", "mailing_list"]
    list_filter: list[str] = ["mailing_list", "send_time", "status", "client"]
    search_fields: list[str] = ["id", "mailing_list", "send_time", "status", "client"]
