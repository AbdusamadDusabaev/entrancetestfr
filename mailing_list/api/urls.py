from django.urls import path
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from .views import ClientAPIView, ClientListAPIView, MailingListAPIView, MailingListListAPIView


app_name: str = "api"


schema_view = get_schema_view(
    openapi.Info(
        title="API Documentation",
        default_version="v1",
        description="API documentation for Mailing List App. More detailed documentation: "
                    "<a href='/api/documentation/'>Detailed Documentation</a>",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="abdusamad.dusabaev@yandex.ru"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
)


urlpatterns: list[path] = [
    path("clients/", ClientListAPIView.as_view(), name="clients"),
    path("client/<int:pk>/", ClientAPIView.as_view(), name="client"),
    path("mailing-lists/", MailingListListAPIView.as_view(), name="mailing-lists"),
    path("mailing-list/<int:pk>/", MailingListAPIView.as_view(), name="mailing-list"),

    path("docs/", schema_view.with_ui("swagger"), name="schema-swagger-ui"),
]
