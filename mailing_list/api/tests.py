import datetime
import freezegun
from typing import Any

from django.test import TestCase
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist

from .models import MailingList, Message, Client, Filter
from .tasks import (
    check_client_time_interval, save_successful_message, set_not_sent_status_to_messages, get_valid_clients,
    send_message, send_new_messages_to_clients, send_messages_to_clients_again, send_messages
)


class MailingListTestCase(TestCase):
    fixtures = ["clients.json", "filters.json", "mailing-lists.json", "messages.json"]

    def test_create_mailing_list_on_valid_input_values(self) -> None:
        error_status_code_msg: str = "Ожидался статус ответа 201. Реальный статус ответа сервера отличается"
        error_response_data_msg: str = "Полученные в ответе данные отличаются от переданных"
        error_database_object_msg: str = "Созданная запись не найдена в базе данных"
        api_url: str = reverse("api:mailing-lists")
        input_values: list[dict[str, str | dict[str, str]]] = [
            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05-05 13:00:00",
                "start_time_interval": "10:00",
                "stop_time_interval": "13:00",
                "text": "Просьба всем сотрудникам подойти в 5 корпус для оглашения важной новости",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": "Сотрудники"
                }
            },

            {
                "start_time": "2023-04-08 00:00:00",
                "stop_time": "2023-05-08 00:00:00",
                "start_time_interval": "08:00",
                "stop_time_interval": "20:00",
                "text": "Срочная новость! В этом месяце проводится акция на все товары",
                "filter": {
                    "mobile_operator_code": "909",
                    "tag": "Клиенты"
                }
            },

            {
                "start_time": "2023-05-06 12:00:00",
                "stop_time": "2023-05-06 22:00:00",
                "start_time_interval": "12:00",
                "stop_time_interval": "22:00",
                "text": "Сегодняшнее мероприятие переносится на завтра в 10:00. До скорой встречи!",
                "filter": {
                    "mobile_operator_code": "977",
                    "tag": "Гости"
                }
            },

            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05-05 13:00:00",
                "stop_time_interval": "13:00",
                "text": "Просьба всем сотрудникам подойти в 5 корпус для оглашения важной новости",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": "Сотрудники"
                }
            },

            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05-05 13:00:00",
                "text": "Просьба всем сотрудникам подойти в 5 корпус для оглашения важной новости",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": "Сотрудники"
                }
            },

            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05-05 13:00:00",
                "start_time_interval": "12:00",
                "text": "Просьба всем сотрудникам подойти в 5 корпус для оглашения важной новости",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": "Сотрудники"
                }
            },

            {
                "start_time": "2023-05-06 12:00",
                "stop_time": "2023-05-06 22:00",
                "start_time_interval": "12:00",
                "stop_time_interval": "22:00",
                "text": "Сегодняшнее мероприятие переносится на завтра в 10:00. До скорой встречи!",
                "filter": {
                    "mobile_operator_code": "977",
                    "tag": "Гости"
                }
            },

            {
                "start_time": "2023-05-06",
                "stop_time": "2023-05-06",
                "start_time_interval": "12:00",
                "stop_time_interval": "22:00",
                "text": "Сегодняшнее мероприятие переносится на завтра в 10:00. До скорой встречи!",
                "filter": {
                    "mobile_operator_code": "977",
                    "tag": "Гости"
                }
            }

        ]

        for input_value in input_values:
            with self.subTest(input_value=input_value):
                response = self.client.post(api_url, data=input_value, content_type="application/json")
                json_response_data: dict[str, str | int | dict[str, str]] = response.json()

                expected_status_code: int = 201
                actual_status_code: int = response.status_code
                self.assertEqual(
                    first=expected_status_code,
                    second=actual_status_code,
                    msg=error_status_code_msg
                )

                expected_created_time: str = datetime.date.today().strftime("%Y-%m-%d")
                expected_start_time_interval: str = datetime.datetime.strptime(
                    input_value.get("start_time_interval", "00:00"), "%H:%M"
                ).strftime("%H:%M:%S")
                expected_stop_time_interval: str = datetime.datetime.strptime(
                    input_value.get("stop_time_interval", "23:59"), "%H:%M"
                ).strftime("%H:%M:%S")
                expected_text: str = input_value["text"]
                expected_mobile_operator_code: str = input_value["filter"]["mobile_operator_code"]
                expected_tag: str = input_value["filter"]["tag"]

                self.assertEqual(
                    first=expected_created_time,
                    second=json_response_data["created_time"],
                    msg=error_response_data_msg
                )
                self.assertEqual(
                    first=expected_start_time_interval,
                    second=json_response_data["start_time_interval"],
                    msg=error_response_data_msg
                )
                self.assertEqual(
                    first=expected_stop_time_interval,
                    second=json_response_data["stop_time_interval"],
                    msg=error_response_data_msg
                )
                self.assertEqual(
                    first=expected_text,
                    second=json_response_data["text"],
                    msg=error_response_data_msg
                )
                self.assertEqual(
                    first=expected_mobile_operator_code,
                    second=json_response_data["filter"]["mobile_operator_code"],
                    msg=error_response_data_msg
                )
                self.assertEqual(
                    first=expected_tag,
                    second=json_response_data["filter"]["tag"],
                    msg=error_response_data_msg
                )

                try:
                    MailingList.objects.get(id=json_response_data["id"])
                except ObjectDoesNotExist:
                    self.assertTrue(False, msg=error_database_object_msg)

    def test_create_filter_object(self) -> None:
        api_url: str = reverse("api:mailing-lists")
        error_status_code_msg: str = "Ожидался статус ответа 201. Реальный статус ответа сервера отличается"
        error_database_object_msg: str = "Созданная запись не найдена в базе данных"
        input_values: list[dict[str, str | dict[str, str]]] = [
            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05-05 13:00:00",
                "start_time_interval": "10:00",
                "stop_time_interval": "13:00",
                "text": "Просьба всем сотрудникам подойти в 5 корпус для оглашения важной новости",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": "Сотрудники"
                }
            },

            {
                "start_time": "2023-04-08 00:00:00",
                "stop_time": "2023-05-08 00:00:00",
                "start_time_interval": "08:00",
                "stop_time_interval": "20:00",
                "text": "Срочная новость! В этом месяце проводится акция на все товары",
                "filter": {
                    "mobile_operator_code": "909",
                    "tag": "Клиенты"
                }
            },

            {
                "start_time": "2023-05-06 12:00:00",
                "stop_time": "2023-05-06 22:00:00",
                "start_time_interval": "12:00",
                "stop_time_interval": "22:00",
                "text": "Сегодняшнее мероприятие переносится на завтра в 10:00. До скорой встречи!",
                "filter": {
                    "mobile_operator_code": "977",
                    "tag": "Гости"
                }
            }

        ]

        for input_value in input_values:
            with self.subTest(input_value=input_value):
                response = self.client.post(api_url, data=input_value, content_type="application/json")

                expected_status_code: int = 201
                actual_status_code: int = response.status_code
                self.assertEqual(
                    first=expected_status_code,
                    second=actual_status_code,
                    msg=error_status_code_msg
                )

                try:
                    operator_code: str = input_value["filter"]["mobile_operator_code"]
                    tag: str = input_value["filter"]["tag"]
                    Filter.objects.get(mobile_operator_code=operator_code, tag=tag)
                except ObjectDoesNotExist:
                    self.assertTrue(False, msg=error_database_object_msg)

    def test_create_mailing_list_on_invalid_input_values(self) -> None:
        error_status_code_msg: str = "Ожидался статус ответа 400. Реальный статус ответа сервера отличается"
        error_amount_objects_msg: str = "Ожидалось, что ни одна запись не будет успешно создана в базе данных"
        api_url: str = reverse("api:mailing-lists")
        input_values: list[dict[str, Any]] = [
            {
                "start_time": True,
                "stop_time": False,
                "start_time_interval": "Some text",
                "stop_time_interval": "Some text again",
                "text": "Такое должно бы выдать ошибку...",
                "filter": {
                    "mobile_operator_code": "Filter option",
                    "tag": "Сотрудники"
                }
            },

            {
                "start_time": True,
                "stop_time": "2023-05-05 13:00:00",
                "start_time_interval": "10:00",
                "stop_time_interval": "13:00",
                "text": "Здесь невалидное значение параметра start_time",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": "Сотрудники"
                }
            },

            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": True,
                "start_time_interval": "10:00",
                "stop_time_interval": "13:00",
                "text": "Здесь невалидное значение параметра stop_time",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": "Сотрудники"
                }
            },

            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05-05 13:00:00",
                "start_time_interval": None,
                "stop_time_interval": "13:00",
                "text": "Здесь явно что-то не так с параметром start_time_interval",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": "Сотрудники"
                }
            },

            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05-05 13:00:00",
                "start_time_interval": "10:00",
                "stop_time_interval": 1234567890,
                "text": "Проблемы в параметре stop_time_interval",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": "Сотрудники"
                }
            },

            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05-05 13:00:00",
                "start_time_interval": "10:00",
                "stop_time_interval": "13:00",
                "text": "Беда с фильтром (параметр mobile_operator_code)",
                "filter": {
                    "mobile_operator_code": "Some Text Here",
                    "tag": "Сотрудники"
                }
            },

            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05-05 13:00:00",
                "start_time_interval": "10:00",
                "stop_time_interval": "13:00",
                "text": "Значение tag в фильтре не совсем корректное",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": None
                }
            },

            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05-05 13:00:00",
                "start_time_interval": "10:00",
                "stop_time_interval": "13:00",
                "text": "Значение tag в фильтре не совсем корректное",
                "filter": {
                    "mobile_operator_code": "929",
                }
            },

            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05-05 13:00:00",
                "start_time_interval": "10:00",
                "stop_time_interval": "13:00",
                "text": "Значение tag в фильтре не совсем корректное",
                "filter": {
                    "tag": None
                }
            },

            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05-05 13:00:00",
                "start_time_interval": "10:00",
                "stop_time_interval": "13:00",
                "text": "Значение tag в фильтре не совсем корректное",
            },

            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05-05 13:00:00",
                "start_time_interval": "10:00",
                "stop_time_interval": "13:00",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": None
                }
            },

            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05-05 13:00:00",
                "start_time_interval": "10:00",
                "text": "Значение tag в фильтре не совсем корректное",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": None
                }
            },

            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05-05 13:00:00",
                "stop_time_interval": "13:00",
                "text": "Значение tag в фильтре не совсем корректное",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": None
                }
            },

            {
                "start_time": "2023-05-05 10:00:00",
                "start_time_interval": "10:00",
                "stop_time_interval": "13:00",
                "text": "Значение tag в фильтре не совсем корректное",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": None
                }
            },

            {
                "stop_time": "2023-05-05 13:00:00",
                "start_time_interval": "10:00",
                "stop_time_interval": "13:00",
                "text": "Значение tag в фильтре не совсем корректное",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": None
                }
            },

            {
                "start_time": "2023-05",
                "stop_time": "2023-05-05 13:00:00",
                "start_time_interval": "10:00",
                "stop_time_interval": "13:00",
                "text": "Значение tag в фильтре не совсем корректное",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": "Some text"
                }
            },

            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05",
                "start_time_interval": "10:00",
                "stop_time_interval": "13:00",
                "text": "Значение tag в фильтре не совсем корректное",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": "Some text"
                }
            },

            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05-05 13:00:00",
                "start_time_interval": "30",
                "stop_time_interval": "13:00",
                "text": "Значение tag в фильтре не совсем корректное",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": "Some text"
                }
            },

            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05-05 13:00:00",
                "start_time_interval": "10:00",
                "stop_time_interval": "-30",
                "text": "Значение tag в фильтре не совсем корректное",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": "Some text"
                }
            },

            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05-05 13:00:00",
                "start_time_interval": "10:00",
                "stop_time_interval": "13:00",
                "text": "Значение tag в фильтре не совсем корректное",
                "filter": {
                    "mobile_operator_code": "99",
                    "tag": "Some text"
                }
            }
        ]

        for input_value in input_values:
            with self.subTest(input_value=input_value):

                expected_status_code: int = 400
                response = self.client.post(api_url, data=input_value, content_type="application/json")
                actual_status_code: int = response.status_code
                self.assertEqual(
                    first=expected_status_code,
                    second=actual_status_code,
                    msg=error_status_code_msg
                )

                mailing_list_objects: list[MailingList] = MailingList.objects.all()
                expected_amount_created_objects: int = 3
                actual_amount_created_objects: int = len(mailing_list_objects)
                self.assertEqual(
                    first=expected_amount_created_objects,
                    second=actual_amount_created_objects,
                    msg=error_amount_objects_msg
                )

    def test_get_mailing_list_objects(self) -> None:
        error_msg: str = "При запросе к серверу были получены не все записи из базы данных"
        api_url: str = reverse("api:mailing-lists")

        expected_response_length: int = len(MailingList.objects.all())
        actual_response_length: int = len(self.client.get(api_url).json())
        self.assertEqual(
            first=expected_response_length,
            second=actual_response_length,
            msg=error_msg
        )

    def test_get_one_mailing_list_object(self) -> None:
        error_msg: str = "Сервер выдал неверные данные при запросе конкретного объекта рассылки"
        mailing_list_objects: list[MailingList] = MailingList.objects.all()
        for mailing_list_object in mailing_list_objects:
            with self.subTest(mailing_list_object=mailing_list_object):
                api_url: str = reverse("api:mailing-list", kwargs={"pk": mailing_list_object.id})
                expected_json_response: dict[str, str | int | dict | list] = {
                    "id": mailing_list_object.id,
                    "start_time": mailing_list_object.start_time.strftime("%Y-%m-%dT%H:%M:%SZ"),
                    "stop_time": mailing_list_object.stop_time.strftime("%Y-%m-%dT%H:%M:%SZ"),
                    "created_time": mailing_list_object.created_time.strftime("%Y-%m-%d"),
                    "start_time_interval": mailing_list_object.start_time_interval.strftime("%H:%M:%S"),
                    "stop_time_interval": mailing_list_object.stop_time_interval.strftime("%H:%M:%S"),
                    "text": mailing_list_object.text,
                    "filter": {
                        "mobile_operator_code": mailing_list_object.filter.mobile_operator_code,
                        "tag": mailing_list_object.filter.tag
                    },
                    "amount_messages": {
                        "Отправлен и доставлен":
                            mailing_list_object.amount_sent_and_received_messages(),
                        "Отправлен, ожидаем ответ от стороннего сервиса":
                            mailing_list_object.amount_sent_and_wait_messages(),
                        "Отправлен, но не доставлен. Сообщение не успело дойти до финального времени рассылки":
                            mailing_list_object.amount_not_sent_messages()
                    },
                    "messages": [
                        {
                            "send_time": message.send_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
                            "status": message.status,
                            "client": {
                                "id": message.client.id,
                                "phone_number": message.client.phone_number,
                                "mobile_operator_code": message.client.mobile_operator_code,
                                "timezone": message.client.timezone,
                                "tag": message.client.tag
                            }
                        }
                        for message in Message.objects.filter(mailing_list=mailing_list_object)
                    ]
                }
                actual_json_response: dict[str, str | int | dict | list] = self.client.get(api_url).json()
                self.assertDictEqual(
                    d1=expected_json_response,
                    d2=actual_json_response,
                    msg=error_msg
                )

    def test_update_mailing_list_object(self) -> None:
        error_msg: str = "Обновление рассылки работает некорректно"
        mailing_list_objects: list[MailingList] = MailingList.objects.all()
        input_values: list[dict[str, str | dict[str, str]]] = [
            {
                "start_time": "2023-05-05 10:00:00",
                "stop_time": "2023-05-05 13:00:00",
                "start_time_interval": "10:00",
                "stop_time_interval": "13:00",
                "text": "Просьба всем сотрудникам подойти в 5 корпус для оглашения важной новости",
                "filter": {
                    "mobile_operator_code": "929",
                    "tag": "Сотрудники"
                }
            },

            {
                "start_time": "2023-04-08 00:00:00",
                "stop_time": "2023-05-08 00:00:00",
                "start_time_interval": "08:00",
                "stop_time_interval": "20:00",
                "text": "Срочная новость! В этом месяце проводится акция на все товары",
                "filter": {
                    "mobile_operator_code": "909",
                    "tag": "Клиенты"
                }
            },

            {
                "start_time": "2023-05-06 12:00:00",
                "stop_time": "2023-05-06 22:00:00",
                "start_time_interval": "12:00",
                "stop_time_interval": "22:00",
                "text": "Сегодняшнее мероприятие переносится на завтра в 10:00. До скорой встречи!",
                "filter": {
                    "mobile_operator_code": "977",
                    "tag": "Гости"
                }
            }

        ]
        for mailing_list_object, input_value in zip(mailing_list_objects, input_values):
            with self.subTest(mailing_list_object=mailing_list_object, input_value=input_value):
                api_url: str = reverse("api:mailing-list", kwargs={"pk": mailing_list_object.pk})

                expected_start_time: str = datetime.datetime.strptime(
                    input_value['start_time'], "%Y-%m-%d %H:%M:%S"
                ).strftime("%Y-%m-%dT%H:%M:%SZ")
                expected_stop_time: str = datetime.datetime.strptime(
                    input_value['stop_time'], "%Y-%m-%d %H:%M:%S"
                ).strftime("%Y-%m-%dT%H:%M:%SZ")
                expected_start_time_interval: str = datetime.datetime.strptime(
                    input_value['start_time_interval'], "%H:%M"
                ).strftime("%H:%M:%S")
                expected_stop_time_interval: str = datetime.datetime.strptime(
                    input_value['stop_time_interval'], "%H:%M"
                ).strftime("%H:%M:%S")
                expected_text: str = input_value["text"]
                expected_mobile_operator_code: str = input_value["filter"]["mobile_operator_code"]
                expected_tag: str = input_value["filter"]["tag"]

                response = self.client.put(path=api_url, data=input_value, content_type="application/json")
                actual_response: dict[str, str | dict[str, str]] = response.json()

                self.assertEqual(
                    first=expected_start_time,
                    second=actual_response["start_time"],
                    msg=error_msg
                )
                self.assertEqual(
                    first=expected_stop_time,
                    second=actual_response["stop_time"],
                    msg=error_msg
                )
                self.assertEqual(
                    first=expected_start_time_interval,
                    second=actual_response["start_time_interval"],
                    msg=error_msg
                )
                self.assertEqual(
                    first=expected_stop_time_interval,
                    second=actual_response["stop_time_interval"],
                    msg=error_msg
                )
                self.assertEqual(
                    first=expected_text,
                    second=actual_response["text"],
                    msg=error_msg
                )
                self.assertEqual(
                    first=expected_mobile_operator_code,
                    second=actual_response["filter"]["mobile_operator_code"],
                    msg=error_msg
                )
                self.assertEqual(
                    first=expected_tag,
                    second=actual_response["filter"]["tag"],
                    msg=error_msg
                )

                expected_status_code: int = 200
                actual_status_code: int = response.status_code
                self.assertEqual(
                    first=expected_status_code,
                    second=actual_status_code,
                    msg=error_msg
                )

    def test_delete_mailing_list_object(self) -> None:
        status_code_error_msg: str = "Ожидался статус ответа 204. Получен другой статус ответа"
        database_error_msg: str = "Объект рассылки не был удален из базы данных"
        mailing_list_objects: list[MailingList] = MailingList.objects.all()
        for mailing_list_object in mailing_list_objects:
            with self.subTest(mailing_list_object=mailing_list_object):
                api_url: str = reverse("api:mailing-list", kwargs={"pk": mailing_list_object.id})
                response = self.client.delete(api_url)

                expected_status_code: int = 204
                actual_status_code: int = response.status_code
                self.assertEqual(
                    first=actual_status_code,
                    second=expected_status_code,
                    msg=status_code_error_msg)

                with self.assertRaises(expected_exception=ObjectDoesNotExist, msg=database_error_msg):
                    MailingList.objects.get(pk=mailing_list_object.id)


class ClientTestCase(TestCase):
    fixtures = ["clients.json", "filters.json", "mailing-lists.json", "messages.json"]

    def test_create_client_on_valid_input_values(self) -> None:
        api_url: str = reverse("api:clients")
        error_status_code_msg: str = "Ожидался статус ответа 201. Реальный статус ответа сервера отличается"
        error_response_data: str = "Полученные в ответе данные отличаются от переданных"
        error_database_object_msg: str = "Созданная запись не найдена в базе данных"
        input_values: list[dict[str, str]] = [
            {
                "phone_number": "79255329134",
                "mobile_operator_code": "925",
                "timezone": "UTC+01:30",
                "tag": "Тестовая запись"
            },

            {
                "phone_number": "79140152032",
                "mobile_operator_code": "914",
                "timezone": "UTC-03:00",
                "tag": "Тестовая запись"
            },

            {
                "phone_number": "79977078020",
                "mobile_operator_code": "997",
                "timezone": "UTC+00:00",
                "tag": "Тестовая запись"
            }
        ]
        for input_value in input_values:
            with self.subTest(input_value=input_value):
                response = self.client.post(api_url, data=input_value, content_type="application/json")
                response_data: dict[str, str | int] = response.json()

                expected_status_code: int = 201
                actual_status_code: int = response.status_code
                self.assertEqual(
                    first=expected_status_code,
                    second=actual_status_code,
                    msg=error_status_code_msg
                )

                self.assertEqual(
                    first=input_value["phone_number"],
                    second=response_data["phone_number"],
                    msg=error_response_data
                )
                self.assertEqual(
                    first=input_value["mobile_operator_code"],
                    second=response_data["mobile_operator_code"],
                    msg=error_response_data
                )
                self.assertEqual(
                    first=input_value["timezone"],
                    second=response_data["timezone"],
                    msg=error_response_data
                )
                self.assertEqual(
                    first=input_value["tag"],
                    second=response_data["tag"],
                    msg=error_response_data
                )

                try:
                    Client.objects.get(pk=response_data["id"])
                except ObjectDoesNotExist:
                    self.assertTrue(False, msg=error_database_object_msg)

    def test_create_client_on_invalid_values(self) -> None:
        api_url: str = reverse("api:clients")
        error_status_code_msg: str = "Ожидался статус ответа 400. Реальный статус ответа сервера отличается"
        error_amount_objects_msg: str = "Ожидалось, что ни одна запись не будет успешно создана в базе данных"
        input_values: list[dict[str, Any]] = [
            {
                "phone_number": "012345678910",
                "mobile_operator_code": True,
                "timezone": "Russia, Moscow",
                "tag": "Тестовая запись"
            },

            {
                "phone_number": "012345678910",
                "mobile_operator_code": "123",
                "timezone": "UTC-03:00",
                "tag": "Тестовая запись"
            },

            {
                "phone_number": "79977078020",
                "mobile_operator_code": "79977078020",
                "timezone": "UTC+00:00",
                "tag": "Тестовая запись"
            },

            {
                "phone_number": "79977078020",
                "mobile_operator_code": "997",
                "timezone": "Russia, Moscow",
                "tag": "Тестовая запись"
            },

            {
                "phone_number": "79255329134",
                "mobile_operator_code": "925",
                "timezone": "UTC+01:30",
            },

            {
                "phone_number": "79255329134",
                "mobile_operator_code": "925",
                "tag": "Тестовая запись"
            },

            {
                "phone_number": "79255329134",
                "timezone": "UTC+01:30",
                "tag": "Тестовая запись"
            },

            {
                "mobile_operator_code": "925",
                "timezone": "UTC+01:30",
                "tag": "Тестовая запись"
            },
        ]
        for input_value in input_values:
            with self.subTest(input_value=input_value):

                expected_status_code: int = 400
                response = self.client.post(api_url, data=input_value, content_type="application/json")
                actual_status_code: int = response.status_code
                self.assertEqual(
                    first=expected_status_code,
                    second=actual_status_code,
                    msg=error_status_code_msg
                )

                client_objects: list[Client] = Client.objects.all()
                expected_amount_created_objects: int = 3
                actual_amount_created_objects: int = len(client_objects)
                self.assertEqual(
                    first=expected_amount_created_objects,
                    second=actual_amount_created_objects,
                    msg=error_amount_objects_msg
                )

    def test_get_client_objects(self) -> None:
        error_msg: str = "При запросе к серверу были получены не все записи из базы данных"
        api_url: str = reverse("api:clients")
        response = self.client.get(api_url)

        expected_response_length: int = len(Client.objects.all())
        actual_response_length: int = len(response.json())
        self.assertEqual(
            first=expected_response_length,
            second=actual_response_length,
            msg=error_msg
        )

    def test_get_one_client_object(self) -> None:
        error_msg: str = "Сервер выдал неверные данные при запросе конкретного объекта клиента"
        client_objects: list[Client] = Client.objects.all()
        for client_object in client_objects:
            with self.subTest(client_object=client_object):
                api_url: str = reverse("api:client", kwargs={"pk": client_object.id})
                expected_json_response: dict[str, str | int] = {
                    "id": client_object.id,
                    "phone_number": client_object.phone_number,
                    "mobile_operator_code": client_object.mobile_operator_code,
                    "timezone": client_object.timezone,
                    "tag": client_object.tag
                }
                actual_json_response: dict[str, str | int] = self.client.get(api_url).json()
                self.assertDictEqual(
                    d1=expected_json_response,
                    d2=actual_json_response,
                    msg=error_msg
                )

    def test_update_client_object(self) -> None:
        error_msg: str = "Обновление информации о клиенте работает некорректно"
        client_objects: list[Client] = Client.objects.all()
        input_values: list[dict[str, str]] = [
            {
                "phone_number": "79255329134",
                "mobile_operator_code": "925",
                "timezone": "UTC+01:30",
                "tag": "Тестовая запись"
            },

            {
                "phone_number": "79140152032",
                "mobile_operator_code": "914",
                "timezone": "UTC-03:00",
                "tag": "Тестовая запись"
            },

            {
                "phone_number": "79977078020",
                "mobile_operator_code": "997",
                "timezone": "UTC+00:00",
                "tag": "Тестовая запись"
            }
        ]
        for client_object, input_value in zip(client_objects, input_values):
            with self.subTest(client=client_object, input_values=input_values):
                api_url: str = reverse("api:client", kwargs={"pk": client_object.id})
                response = self.client.put(api_url, data=input_value, content_type="application/json")
                response_json_data: dict[str, str | int] = response.json()

                self.assertEqual(
                    first=client_object.id,
                    second=response_json_data["id"],
                    msg=error_msg
                )
                self.assertEqual(
                    first=input_value["phone_number"],
                    second=response_json_data["phone_number"],
                    msg=error_msg
                )
                self.assertEqual(
                    first=input_value["mobile_operator_code"],
                    second=response_json_data['mobile_operator_code'],
                    msg=error_msg
                )
                self.assertEqual(
                    first=input_value["timezone"],
                    second=response_json_data["timezone"],
                    msg=error_msg
                )
                self.assertEqual(
                    first=input_value["tag"],
                    second=response_json_data["tag"],
                    msg=error_msg
                )

                expected_status_code: int = 200
                actual_status_code: int = response.status_code
                self.assertEqual(
                    first=expected_status_code,
                    second=actual_status_code,
                    msg=error_msg
                )

    def test_delete_client_object(self) -> None:
        status_code_error_msg: str = "Ожидался статус ответа 204. Получен другой статус ответа"
        database_error_msg: str = "Объект клиента не был удален из базы данных"
        client_objects: list[Client] = Client.objects.all()
        for client_object in client_objects:
            with self.subTest(client_object=client_object):
                api_url: str = reverse("api:client", kwargs={"pk": client_object.id})
                response = self.client.delete(api_url)

                expected_status_code: int = 204
                actual_status_code: int = response.status_code
                self.assertEqual(
                    first=expected_status_code,
                    second=actual_status_code,
                    msg=status_code_error_msg
                )

                with self.assertRaises(expected_exception=ObjectDoesNotExist, msg=database_error_msg):
                    Client.objects.get(pk=client_object.id)


class CelerySendMessageTaskTestCase(TestCase):
    fixtures = ["clients.json", "filters.json", "mailing-lists.json", "messages.json"]

    @freezegun.freeze_time("2023-05-07 18:00")
    def test_check_valid_client_time_interval(self) -> None:
        error_msg: str = "Получен неожиданный результат"
        mailing_list_objects: list[MailingList] = MailingList.objects.all()
        client_objects: list[Client] = Client.objects.all()
        for mailing_list_object, client_object in zip(mailing_list_objects, client_objects):
            with self.subTest(mailing_list_object=mailing_list_object, client_object=client_object):
                result: bool = check_client_time_interval(client=client_object, mailing_list_object=mailing_list_object)
                self.assertTrue(result, msg=error_msg)

    def test_send_message(self) -> None:
        error_msg: str = "Отправка сообщения не удалась"
        message_objects: list[Message] = Message.objects.all()
        for message_object in message_objects:
            with self.subTest(message_object=message_object):
                result: bool = send_message(message=message_object)
                self.assertTrue(result, msg=error_msg)

    @freezegun.freeze_time("2023-05-07 18:00")
    def test_check_client_time_interval(self) -> None:
        error_msg: str = "Неожиданный результат проверки клиентского интервала времени"
        mailing_list_objects: list[MailingList] = MailingList.objects.all()
        for mailing_list_object in mailing_list_objects:
            operator_code: str = mailing_list_object.filter.mobile_operator_code
            tag: str = mailing_list_object.filter.tag
            clients: list[Client] = Client.objects.filter(mobile_operator_code=operator_code).filter(tag=tag).all()
            for client in clients:
                with self.subTest(mailing_list_object=mailing_list_object, client=client):
                    result: bool = check_client_time_interval(client=client, mailing_list_object=mailing_list_object)
                    self.assertTrue(result, msg=error_msg)

    @freezegun.freeze_time("2023-05-07 18:00")
    def test_get_valid_clients(self) -> None:
        error_msg: str = "Неожиданное количество валидных клиентов"
        mailing_list_objects: list[MailingList] = MailingList.objects.all()
        for mailing_list_object in mailing_list_objects:
            with self.subTest(mailing_list_object=mailing_list_object):
                result: list[Client] = get_valid_clients(mailing_list_object=mailing_list_object)
                expected_amount_valid_clients: int = 1
                result_amount_valid_clients: int = len(result)
                self.assertEqual(
                    first=expected_amount_valid_clients,
                    second=result_amount_valid_clients,
                    msg=error_msg
                )

    def test_save_successful_message(self) -> None:
        error_msg: str = 'Изменение статуса на "Отправлен и доставлен" работает некорректно'
        mailing_list_objects: list[MailingList] = MailingList.objects.all()
        for mailing_list_object in mailing_list_objects:
            operator_code: str = mailing_list_object.filter.mobile_operator_code
            tag: str = mailing_list_object.filter.tag
            clients: list[Client] = Client.objects.filter(mobile_operator_code=operator_code).filter(tag=tag).all()
            for client in clients:
                with self.subTest(mailing_list_object=mailing_list_object, client=client):
                    message: Message = Message(
                        send_time=datetime.datetime.now(),
                        status="Отправлен, ожидаем ответ от стороннего сервиса",
                        mailing_list=mailing_list_object,
                        client=client
                    )
                    message.save()
                    save_successful_message(message=message)
                    expected_status: str = "Отправлен и доставлен"
                    actual_status: str = message.status
                    self.assertEqual(
                        first=expected_status,
                        second=actual_status,
                        msg=error_msg
                    )

                    expected_send_time: str = datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S")
                    actual_send_time: str = message.send_time.strftime("%Y-%m-%d %H-%M-%S")
                    self.assertEqual(
                        first=expected_send_time,
                        second=actual_send_time,
                        msg=error_msg
                    )

    def test_set_not_sent_status_to_messages(self) -> None:
        error_msg: str = 'Статус сообщения не был изменен на "Отправлен, но не доставлен. ' \
                         'Сообщение не успело дойти до финального времени рассылки"'
        expected_status: str = "Отправлен, но не доставлен. Сообщение не успело дойти до финального времени рассылки"
        mailing_list_objects: list[MailingList] = MailingList.objects.all()
        for mailing_list_object in mailing_list_objects:
            operator_code: str = mailing_list_object.filter.mobile_operator_code
            tag: str = mailing_list_object.filter.tag
            now: datetime.datetime = datetime.datetime.now()
            clients: list[Client] = Client.objects.filter(mobile_operator_code=operator_code).filter(tag=tag).all()
            clients_dict: dict[str, tuple[bool, int]] = {
                client.phone_number: (False, Message.objects.get(client=client, mailing_list=mailing_list_object).pk)
                for client in clients
            }
            set_not_sent_status_to_messages(clients=clients, mailing_list_object=mailing_list_object,
                                            clients_dict=clients_dict, now=now)

            for client in clients:
                messages: list[Message] = Message.objects.filter(
                    client=client, mailing_list=mailing_list_object, send_time=now
                ).all()

                for message in messages:
                    actual_status: str = message.status
                    self.assertEqual(
                        first=expected_status,
                        second=actual_status,
                        msg=error_msg
                    )

    @freezegun.freeze_time("2023-05-07 18:00")
    def test_send_new_messages_to_client(self) -> None:
        error_msg: str = "Ожидаемое количество созданных сообщений при первой отправке не соответствует реальному"
        mailing_list_objects: list[MailingList] = MailingList.objects.all()
        for mailing_list in mailing_list_objects:
            operator_code: str = mailing_list.filter.mobile_operator_code
            tag: str = mailing_list.filter.tag
            clients: list[Client] = Client.objects.filter(mobile_operator_code=operator_code).filter(tag=tag).all()
            now: datetime.datetime = datetime.datetime.now()
            clients_dict: dict[str, tuple[bool, int]] = send_new_messages_to_clients(
                clients=clients, mailing_list_object=mailing_list, now=now
            )

            messages: list[Message] = Message.objects.filter(send_time=now).filter(mailing_list=mailing_list).all()
            expected_messages_amount: int = len(clients)
            actual_messages_amount: int = len(messages)
            self.assertEqual(
                first=expected_messages_amount,
                second=actual_messages_amount,
                msg=error_msg
            )

            for client_info in clients_dict.values():
                self.assertEqual(first=True, second=client_info[0])

    @freezegun.freeze_time("2023-05-07 18:00")
    def test_send_messages_to_client_again(self) -> None:
        mailing_list_objects: list[MailingList] = MailingList.objects.all()
        for mailing_list_object in mailing_list_objects:
            operator_code: str = mailing_list_object.filter.mobile_operator_code
            tag: str = mailing_list_object.filter.tag
            clients: list[Client] = Client.objects.filter(mobile_operator_code=operator_code).filter(tag=tag).all()
            now: datetime.datetime = datetime.datetime.now()
            clients_dict: dict[str, tuple[bool, int]] = send_new_messages_to_clients(
                clients=clients, mailing_list_object=mailing_list_object, now=now
            )
            new_clients_dict: dict[str, tuple[bool, int]] = send_messages_to_clients_again(
                clients_dict=clients_dict, clients=clients, mailing_list_object=mailing_list_object
            )
            self.assertTrue(clients_dict == new_clients_dict)

    @freezegun.freeze_time("2023-05-07 18:00")
    def test_send_messages_task(self) -> None:
        error_msg: str = "При выполнении celery задачи send_messages не было создано новых сообщений в базе данных"
        mailing_list_objects: list[MailingList] = MailingList.objects.all()
        for mailing_list_object in mailing_list_objects:
            old_amount_messages: int = len(Message.objects.filter(mailing_list=mailing_list_object).all())
            expected_new_amount_messages: int = old_amount_messages + 1

            send_messages(mailing_list_id=mailing_list_object.id, stop_time=mailing_list_object.stop_time)
            actual_new_amount_messages: int = len(Message.objects.filter(mailing_list=mailing_list_object).all())

            self.assertEqual(
                first=expected_new_amount_messages,
                second=actual_new_amount_messages,
                msg=error_msg
            )
