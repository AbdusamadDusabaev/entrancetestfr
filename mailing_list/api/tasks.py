import datetime
import time
import requests

from mailing_list.celery import app
from .models import MailingList, Client, Message
from .logs import write_mailing_list_log, write_message_log


API_KEY: str = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTQ4Mzc3MzYsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS9CdWxkb2cxNzAyIn0.EK5R6DpTzFJhnvVWUZMhvIUOhaosE0EMefILGnDrwj0"
PAUSE_TIME: int = 60


@app.task
def send_messages_task(mailing_list_id: int, stop_time: str) -> None:
    send_messages(mailing_list_id=mailing_list_id, stop_time=stop_time)


def send_messages(mailing_list_id: int, stop_time: str):
    date_string_stop_time: str = str(stop_time).replace("Z", "").replace("T", " ")
    mailing_list_object: MailingList = MailingList.objects.get(pk=mailing_list_id)
    clients: list[Client] = get_valid_clients(mailing_list_object=mailing_list_object)
    now: datetime.datetime = datetime.datetime.now()

    write_mailing_list_log(mailing_list_id=mailing_list_id, log="Отправляем сообщения по рассылке")
    clients_dict: dict[str, tuple[bool, int]] = send_new_messages_to_clients(
        clients=clients,
        now=now,
        mailing_list_object=mailing_list_object
    )
    result_messages: list[bool] = [value[0] for key, value in clients_dict.items()]
    all_messages_are_sent: bool = all(result_messages)
    if all_messages_are_sent:
        write_mailing_list_log(mailing_list_id=mailing_list_id, log="Все сообщения успешно отправлены")
        return None

    while True:
        write_mailing_list_log(
            mailing_list_id=mailing_list_id,
            log=f"Успешно отправленных сообщений: {result_messages.count(True)}. "
                f"Не удалось отправить: {len(result_messages)}. Повторная попытка через {PAUSE_TIME} секунд"
        )
        time.sleep(PAUSE_TIME)
        if datetime.datetime.now() >= datetime.datetime.strptime(date_string_stop_time, "%Y-%m-%d %H:%M:%S"):
            write_mailing_list_log(
                mailing_list_id=mailing_list_id,
                log=f"Время рассылки закончилось. "
                    f"Успешно отправленных сообщений: {result_messages.count(True)}. "
                    f"Не удалось отправить: {len(result_messages)}"
            )
            set_not_sent_status_to_messages(
                clients=clients,
                mailing_list_object=mailing_list_object,
                now=now,
                clients_dict=clients_dict
            )
            break

        clients_dict: dict[str, tuple[bool, int]] = send_messages_to_clients_again(
            clients=clients,
            clients_dict=clients_dict,
            mailing_list_object=mailing_list_object
        )
        result_messages: list[bool] = [value[0] for key, value in clients_dict.items()]
        all_messages_are_sent: bool = all(result_messages)

        if all_messages_are_sent:
            write_mailing_list_log(mailing_list_id=mailing_list_id, log="Все сообщения успешно отправлены")
            break


def get_valid_clients(mailing_list_object: MailingList) -> list[Client]:
    mobile_operator_code: str = mailing_list_object.filter.mobile_operator_code
    tag: str = mailing_list_object.filter.tag

    all_client: list[Client] = Client.objects.filter(mobile_operator_code=mobile_operator_code).filter(tag=tag).all()
    valid_clients: list[Client] = list()
    for client in all_client:
        if check_client_time_interval(client=client, mailing_list_object=mailing_list_object):
            valid_clients.append(client)
    return valid_clients


def send_new_messages_to_clients(clients: list[Client], mailing_list_object: MailingList,
                                 now: datetime.datetime) -> dict[str, tuple[bool, int]]:
    clients_dict: dict[str, tuple[bool, int]] = {client.phone_number: (False, 0) for client in clients}

    for client in clients:
        if check_client_time_interval(client=client, mailing_list_object=mailing_list_object):
            message = Message(
                send_time=now,
                status="Отправлен, ожидаем ответ от стороннего сервиса",
                mailing_list=mailing_list_object,
                client=client
            )
            message.save()
            write_message_log(message_id=message.pk, log=f"Создано новое сообщение: {message.log_str()}")
            message_is_sent: bool = send_message(message=message)
            clients_dict[client.phone_number]: tuple[bool, int] = (message_is_sent, message.pk)

    return clients_dict


def send_messages_to_clients_again(clients_dict: dict[str, tuple[bool, int]], clients: list[Client],
                                   mailing_list_object: MailingList) -> dict[str, tuple[bool, int]]:
    for client in clients:
        if (clients_dict[client.phone_number][0] is False and
                check_client_time_interval(client=client, mailing_list_object=mailing_list_object)):
            message: Message = Message.objects.get(pk=clients_dict[client.phone_number][1])
            write_message_log(message_id=message.pk, log="Повторная попытка доставить сообщение")
            message_is_sent: bool = send_message(message=message)
            clients_dict[client.phone_number]: tuple[bool, int] = (message_is_sent, message.pk)

    return clients_dict


def set_not_sent_status_to_messages(clients: list[Client], mailing_list_object: MailingList,
                                    clients_dict: dict[str, tuple[bool, int]], now: datetime.datetime) -> None:
    for client in clients:
        if clients_dict[client.phone_number][0] is False:
            message: Message = Message.objects.get_or_create(pk=clients_dict[client.phone_number][1])[0]

            message.send_time = now
            message.status = "Отправлен, но не доставлен. Сообщение не успело дойти до финального времени рассылки"
            message.mailing_list = mailing_list_object
            message.client = client
            message.save()

            write_message_log(
                message_id=message.pk,
                log='Время отправки сообщений вышло. Не удалось отправить сообщение. '
                    f'Статус изменен на "{message.status}"'
            )


def send_message(message: Message) -> bool:
    post_data: dict[str, int | str] = {
        "id": message.pk,
        "phone": message.client.phone_number,
        "text": message.mailing_list.text
    }
    url: str = f"https://probe.fbrq.cloud/v1/send/{message.pk}"
    headers: dict[str, str] = {"authorization": f"Bearer {API_KEY}"}
    response: requests.Response = requests.post(url=url, json=post_data, headers=headers)

    if response.status_code == 200:
        save_successful_message(message=message)
        write_message_log(
            message_id=message.pk,
            log='Сообщение успешно доставлено. Статус изменен на "Отправлен и доставлен". '
                f'Ответ от стороннего сервера: {response.content}. Статус-код ответа: {response.status_code}'
        )
        return True

    write_message_log(
        message_id=message.pk,
        log=f"Не удалось доставить сообщение. "
            f"Ответ от стороннего сервера: {response.content}. Статус-код ответа: {response.status_code}"
    )
    return False


def save_successful_message(message: Message) -> None:
    message.status = "Отправлен и доставлен"
    message.send_time = datetime.datetime.now()
    message.save()


def check_client_time_interval(client: Client, mailing_list_object: MailingList) -> bool:
    start_time_interval: datetime.time = mailing_list_object.start_time_interval
    stop_time_interval: datetime.time = mailing_list_object.stop_time_interval

    utc_shift: str = str(client.timezone).replace("UTC", "")
    tz: datetime.tzinfo = datetime.datetime.strptime(utc_shift, "%z").tzinfo
    client_time_in_my_time: datetime.datetime = datetime.datetime.now().astimezone(tz=tz)

    if stop_time_interval > client_time_in_my_time.time() > start_time_interval:
        return True
    return False
