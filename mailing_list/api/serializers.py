import datetime

from rest_framework.serializers import ModelSerializer

from .tasks import send_messages_task
from .models import Client, MailingList, Filter
from .logs import write_client_log, write_mailing_list_log


class FilterSerializer(ModelSerializer):
    class Meta:
        model: Filter = Filter
        fields: list[str] = ["mobile_operator_code", "tag"]


class ClientSerializer(ModelSerializer):
    class Meta:
        model: Client = Client
        fields: list[str] = ["id", "phone_number", "mobile_operator_code", "timezone", "tag"]

    def create(self, validated_data: dict) -> Client:
        client_object: Client = Client(
            phone_number=validated_data["phone_number"],
            mobile_operator_code=validated_data["mobile_operator_code"],
            timezone=validated_data["timezone"],
            tag=validated_data["tag"]
        )
        client_object.save()

        write_client_log(client_id=client_object.pk, log=f"Создан новый клиент: {client_object.log_str()}")

        return client_object

    def update(self, instance: Client, validated_data: dict) -> Client:
        log: str = f"Данные клиента обновлены: [{instance.log_str()}] -> "

        instance.phone_number = validated_data["phone_number"]
        instance.mobile_operator_code = validated_data["mobile_operator_code"]
        instance.timezone = validated_data["timezone"]
        instance.tag = validated_data["tag"]
        instance.save()

        log: str = log + f"[{instance.log_str()}]"
        write_client_log(client_id=instance.pk, log=log)

        return instance


class MailingListSerializer(ModelSerializer):
    filter: FilterSerializer = FilterSerializer(read_only=False)

    class Meta:
        model: MailingList = MailingList
        fields: list[str] = [
            "id", "start_time", "stop_time", "created_time", "start_time_interval",
            "stop_time_interval", "text", "filter", "amount_messages", "messages"
        ]

    def create(self, validated_data: dict) -> MailingList:
        filter_object: Filter = Filter.objects.get_or_create(
            mobile_operator_code=validated_data["filter"]["mobile_operator_code"],
            tag=validated_data["filter"]["tag"]
        )[0]

        if "start_time_interval" in validated_data.keys() and "stop_time_interval" in validated_data.keys():
            mailing_list_object: MailingList = MailingList(
                start_time=validated_data["start_time"],
                stop_time=validated_data["stop_time"],
                start_time_interval=validated_data["start_time_interval"],
                stop_time_interval=validated_data["stop_time_interval"],
                text=validated_data["text"],
                filter=filter_object
            )
        elif "start_time_interval" in validated_data.keys():
            mailing_list_object: MailingList = MailingList(
                start_time=validated_data["start_time"],
                stop_time=validated_data["stop_time"],
                start_time_interval=validated_data["start_time_interval"],
                text=validated_data["text"],
                filter=filter_object
            )
        elif "stop_time_interval" in validated_data.keys():
            mailing_list_object: MailingList = MailingList(
                start_time=validated_data["start_time"],
                stop_time=validated_data["stop_time"],
                stop_time_interval=validated_data["stop_time_interval"],
                text=validated_data["text"],
                filter=filter_object
            )
        else:
            mailing_list_object: MailingList = MailingList(
                start_time=validated_data["start_time"],
                stop_time=validated_data["stop_time"],
                text=validated_data["text"],
                filter=filter_object
            )

        mailing_list_object.save()

        write_mailing_list_log(
            mailing_list_id=mailing_list_object.pk,
            log=f"Создана новая рассылка: {mailing_list_object.log_str()}"
        )

        if validate_start_and_stop_times(start_time=mailing_list_object.start_time,
                                         stop_time=mailing_list_object.stop_time):
            send_messages_task.apply_async(
                [mailing_list_object.pk, mailing_list_object.stop_time],
                eta=mailing_list_object.start_time,
                expires=mailing_list_object.stop_time
            )

        return mailing_list_object

    def update(self, instance: MailingList, validated_data: dict) -> MailingList:
        filter_object: Filter = Filter.objects.get_or_create(
            mobile_operator_code=validated_data["filter"]["mobile_operator_code"],
            tag=validated_data["filter"]["tag"]
        )[0]

        log: str = f"Информация по рассылке обновлена: [{instance.log_str()}] -> "

        instance.filter = filter_object
        instance.start_time = validated_data["start_time"]
        instance.stop_time = validated_data["stop_time"]
        instance.start_time_interval = validated_data["start_time_interval"]
        instance.stop_time_interval = validated_data["stop_time_interval"]
        instance.text = validated_data["text"]
        instance.save()

        log: str = log + f"[{instance.log_str()}]"
        write_mailing_list_log(mailing_list_id=instance.pk, log=log)

        if validate_start_and_stop_times(start_time=instance.start_time, stop_time=instance.stop_time):
            send_messages_task.apply_async(
                [instance.pk, instance.stop_time],
                eta=instance.start_time,
                expires=instance.stop_time
            )

        return instance


def validate_start_and_stop_times(start_time: MailingList.start_time, stop_time: MailingList.stop_time) -> bool:
    date_string_start_time: str = str(start_time).split("+")[0]
    date_string_stop_time: str = str(stop_time).split("+")[0]
    date_format_stop_time: datetime.datetime = datetime.datetime.strptime(date_string_stop_time, "%Y-%m-%d %H:%M:%S")
    if date_string_stop_time > date_string_start_time and (date_format_stop_time > datetime.datetime.now()):
        return True
    return False
