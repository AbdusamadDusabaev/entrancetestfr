from django.http import HttpRequest

from rest_framework.generics import GenericAPIView
from rest_framework import mixins
from rest_framework.serializers import Serializer
from rest_framework.response import Response

from .models import MailingList, Client
from .serializers import ClientSerializer, MailingListSerializer
from .logs import write_client_log, write_mailing_list_log


class ClientAPIView(mixins.UpdateModelMixin, mixins.DestroyModelMixin, mixins.RetrieveModelMixin, GenericAPIView):
    queryset: list = Client.objects.all()
    serializer_class: Serializer = ClientSerializer

    def get(self, request: HttpRequest, *args, **kwargs) -> Response:
        """API для получения информации о клиенте по его ID"""
        log: str = "Пользователь сделал запрос на получение детальной информации о клиенте"
        write_client_log(client_id=request.GET.get("pk"), log=log)
        return self.retrieve(request=request, *args, **kwargs)

    def put(self, request: HttpRequest, *args, **kwargs) -> Response:
        """
        API для обновления информации о клиенте по его ID
        """
        log: str = "Пользователь сделал запрос на обновление информации о клиенте"
        write_client_log(client_id=request.GET.get("pk"), log=log)
        return self.update(request=request, *args, **kwargs)

    def delete(self, request: HttpRequest, *args, **kwargs) -> Response:
        """API для удаления клиента из базы по его ID"""
        log: str = "Пользователь сделал запрос на удаление клиента из базы"
        write_client_log(client_id=request.GET.get("pk"), log=log)
        return self.destroy(request=request, *args, **kwargs)


class ClientListAPIView(mixins.ListModelMixin, mixins.CreateModelMixin, GenericAPIView):
    queryset: list = Client.objects.all()
    serializer_class: Serializer = ClientSerializer

    def get(self, request: HttpRequest, *args, **kwargs) -> Response:
        """API для получения информации обо всех клиентах"""
        log: str = "Пользователь сделал запрос на получение информации обо всех клиентах"
        write_client_log(client_id="all", log=log)
        return self.list(request=request, *args, **kwargs)

    def post(self, request: HttpRequest, *args, **kwargs) -> Response:
        """API для создания нового клиента"""
        log: str = "Пользователь сделал запрос на создание нового клиента в базе"
        write_client_log(client_id="all", log=log)
        return self.create(request=request, *args, **kwargs)


class MailingListAPIView(mixins.UpdateModelMixin, mixins.DestroyModelMixin, mixins.RetrieveModelMixin, GenericAPIView):
    queryset: list = MailingList.objects.all()
    serializer_class: Serializer = MailingListSerializer

    def get(self, request: HttpRequest, *args, **kwargs) -> Response:
        """API для получения информации по рассылке по ее ID"""
        log: str = "Пользователь сделал запрос на получение детальной информации по рассылке"
        write_mailing_list_log(mailing_list_id=request.GET.get("pk"), log=log)
        return self.retrieve(request=request, *args, **kwargs)

    def put(self, request: HttpRequest, *args, **kwargs) -> Response:
        """API для обновления информации по рассылке по ее ID"""
        log: str = "Пользователь сделал запрос на обновление информации по рассылке"
        write_mailing_list_log(mailing_list_id=request.GET.get("pk"), log=log)
        return self.update(request=request, *args, **kwargs)

    def delete(self, request: HttpRequest, *args, **kwargs) -> Response:
        """API для удаления рассылки из базы по ее ID"""
        log: str = "Пользователь сделал запрос на удаление рассылки из базы"
        write_mailing_list_log(mailing_list_id=request.GET.get("pk"), log=log)
        return self.destroy(request=request, *args, **kwargs)


class MailingListListAPIView(mixins.ListModelMixin, mixins.CreateModelMixin, GenericAPIView):
    queryset: list = MailingList.objects.all()
    serializer_class: Serializer = MailingListSerializer

    def get(self, request: HttpRequest, *args, **kwargs) -> Response:
        """API для получения информации обо всех рассылках"""
        log: str = "Пользователь сделал запрос на получений информации обо всех рассылках"
        write_mailing_list_log(mailing_list_id="all", log=log)
        return self.list(request=request, *args, **kwargs)

    def post(self, request: HttpRequest, *args, **kwargs) -> Response:
        """API для создания новой рассылки"""
        log: str = "Пользователь сделал запрос на создание новой рассылки в базе"
        write_mailing_list_log(mailing_list_id="all", log=log)
        return self.create(request=request, *args, **kwargs)
