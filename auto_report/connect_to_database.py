import datetime
import os
import pathlib

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, relationship, Mapped
from sqlalchemy.orm.relationships import Relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, DateTime, Date, Integer, String, ForeignKey

path_to_database: str = os.path.abspath(pathlib.Path("..", "mailing_list", "db.sqlite3"))
engine = create_engine(f"sqlite:///{path_to_database}")
Session = sessionmaker(bind=engine)
session = Session()
Base = declarative_base()


class Client(Base):
    __tablename__ = "api_client"

    id: Mapped[int] = Column(Integer, primary_key=True)
    mobile_operator_code: Mapped[str] = Column(String)
    phone_number: Mapped[str] = Column(String)
    tag: Mapped[str] = Column(String)
    timezone: Mapped[str] = Column(String)


class Filter(Base):
    __tablename__ = "api_filter"

    id: Mapped[int] = Column(Integer, primary_key=True)
    mobile_operator_code: Mapped[str] = Column(String)
    tag: Mapped[str] = Column(String)


class MailingList(Base):
    __tablename__ = "api_mailinglist"

    id: Mapped[int] = Column(Integer, primary_key=True)
    start_time: Mapped[datetime.datetime] = Column(DateTime)
    text: Mapped[str] = Column(String)
    filter_id: Mapped[int] = Column(ForeignKey("api_filter.id"))
    filter: Mapped[Relationship] = relationship("Filter", backref="mailing_lists")
    stop_time: Mapped[datetime.datetime] = Column(DateTime)
    created_time: Mapped[datetime.date] = Column(Date)


class Message(Base):
    __tablename__ = "api_message"

    id: Mapped[int] = Column(Integer, primary_key=True)
    client_id: Mapped[int] = Column(ForeignKey("api_client.id"))
    client: Mapped[Relationship] = relationship("Client", backref="messages")
    mailing_list_id: Mapped[int] = Column(ForeignKey("api_mailinglist.id"))
    mailing_list: Mapped[Relationship] = relationship("MailingList", backref="messages")
    send_time: Mapped[datetime.datetime] = Column(DateTime)
    status: Mapped[str] = Column(String)
