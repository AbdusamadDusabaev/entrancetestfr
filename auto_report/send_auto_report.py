import datetime
import json
import os
import pathlib
import time

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
from email.header import Header

import schedule

from connect_to_database import session, MailingList, Message
from config import email_address_list, email_login, email_password, host, port, time_to_send


def main():
    print("[INFO] Автоматическая рассылка отчетов по API MailingList запущена")
    write_log(log="Рассылка отчета по API MailingList запущена")
    schedule.every().day.at(time_to_send).do(send_emails)
    while True:
        schedule.run_pending()
        time.sleep(1)


def send_emails():
    write_log(log="Отправляем электронные сообщения")
    server: smtplib.SMTP = smtplib.SMTP(host=host, port=port)
    server.starttls()
    server.ehlo()
    server.login(user=email_login, password=email_password)

    amount_mailing_lists, amount_messages, amount_clients = get_and_write_data_to_files_from_database()
    message_content: str = f"""
    <h1>Отчет по рассылкам за {datetime.date.today()} (API MailingList)</h1>
    <h3> Краткий отчет </h3>
    <p>Количество обработанных рассылок: {amount_mailing_lists}</p>
    <p>Количество обработанных сообщений во всех рассылках: {amount_messages}</p>
    <p>Количество обработанных клиентов во всех рассылках: {amount_clients}</p>
    <br>
    <p><b><i>Более подробный отчет вы можете посмотреть в формате json в прикрепленных файлах</i></b></p>
    """

    for email_address in email_address_list:
        message: MIMEMultipart = MIMEMultipart()
        message["From"]: str = email_login
        message["To"]: str = email_address
        message['Subject']: Header = Header(f'Отчет по API MailingList за {datetime.date.today()}', 'utf-8')
        message.attach(MIMEText(message_content, 'html',  "utf-8"))

        for file_object in os.listdir("results"):
            filename: str = os.path.basename(file_object)
            with open(f"results/{filename}", "rb") as file:
                file_content: bytes = file.read()
            file_message: MIMEApplication = MIMEApplication(file_content, "application/json")
            file_message.add_header("content-disposition", "attachment", filename=filename)
            message.attach(file_message)

        server.send_message(msg=message)
        write_log(log=f"Сообщение отправлено по адресу: {email_address}")

    write_log(log="Рассылка отчета завершена!\n")


def get_and_write_data_to_files_from_database():
    write_log(log="Формируем отчет")
    today: datetime.date = datetime.date.today()
    mailing_lists: list = session.query(MailingList).filter(MailingList.created_time == today).all()
    messages: list = session.query(Message).join(MailingList).filter(MailingList.created_time == today).all()
    clients: set = {message.client for message in messages}

    amount_mailing_lists: int = len(mailing_lists)
    amount_messages: int = len(messages)
    amount_clients: int = len(clients)

    clients_info: list[dict[str, str | int]] = [
        {
            "id": client.id,
            "phone_number": client.phone_number,
            "mobile_operator_code": client.mobile_operator_code,
            "tag": client.tag,
            "timezone": client.timezone
        }
        for client in clients
    ]

    clients_info_path: pathlib.Path = pathlib.Path("results", "clients.json")
    with open(clients_info_path, "w", encoding="utf-8") as file:
        json.dump(clients_info, file, indent=4, ensure_ascii=False)

    mailing_lists_info: list[dict[str, int | str | dict]] = [
        {
            "id": mailing_list.id,
            "created_time": str(mailing_list.created_time),
            "start_time": str(mailing_list.start_time),
            "stop_time": str(mailing_list.stop_time),
            "text": str(mailing_list.text),
            "filter": {
                "id": mailing_list.filter.id,
                "mobile_operator_code": mailing_list.filter.mobile_operator_code,
                "tag": mailing_list.filter.tag
            },
            "messages": [
                {
                    "id": message.id,
                    "send_time": str(message.send_time),
                    "status": message.status,
                    "client": {
                        "id": message.client.id,
                        "phone_number": message.client.phone_number,
                        "mobile_operator_code": message.client.mobile_operator_code,
                        "tag": message.client.tag,
                        "timezone": message.client.timezone
                    }
                }
                for message in mailing_list.messages
            ]
        }
        for mailing_list in mailing_lists
    ]

    mailing_lists_info_path: pathlib.Path = pathlib.Path("results", "mailing-lists.json")
    with open(mailing_lists_info_path, "w", encoding="utf-8") as file:
        json.dump(mailing_lists_info, file, indent=4, ensure_ascii=False)

    messages_info: list[dict[str, int | str | dict]] = [
        {
            "id": message.id,
            "send_time": str(message.send_time),
            "status": message.status,
            "client": {
                "id": message.client.id,
                "phone_number": message.client.phone_number,
                "mobile_operator_code": message.client.mobile_operator_code,
                "tag": message.client.tag,
                "timezone": message.client.timezone
            },
            "mailing_list": {
                "id": message.mailing_list.id,
                "created_time": str(message.mailing_list.created_time),
                "start_time": str(message.mailing_list.start_time),
                "stop_time": str(message.mailing_list.stop_time),
                "text": message.mailing_list.text,
                "filter": {
                    "id": message.mailing_list.filter.id,
                    "mobile_operator_code": message.mailing_list.filter.mobile_operator_code,
                    "tag": message.mailing_list.filter.tag
                }
            }
        }
        for message in messages
    ]

    messages_info_path: pathlib.Path = pathlib.Path("results", "messages.json")
    with open(messages_info_path, "w", encoding="utf-8") as file:
        json.dump(messages_info, file, indent=4, ensure_ascii=False)

    return amount_mailing_lists, amount_messages, amount_clients


def write_log(log: str) -> None:
    today: datetime.date = datetime.date.today()
    log_path: pathlib.Path = pathlib.Path("logs", f"{today}.txt")
    with open(log_path, "a", encoding="utf-8") as file:
        file.write(f"[{datetime.datetime.now()}] {log}\n")


if __name__ == '__main__':
    main()
